package com.learning.dmitry.learning;

import android.app.TimePickerDialog;
import android.content.Intent;
import android.database.Cursor;
import android.media.RingtoneManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TimePicker;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class AlarmForm extends AppCompatActivity {

    private AlarmModel _model;
    private int _position;
    private Button _btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alarm_form);

        _btn = (Button) findViewById(R.id.btnDate);

        _readIntent();
        _setDateToButton(_model);
        _createSongSpinner();
    }

    private void _createSongSpinner() {
        Spinner spinner = (Spinner) findViewById(R.id.songs_list);
        ArrayList<AlarmSound> sounds = _getSounds();
        ArrayAdapter<AlarmSound> adapter = new ArrayAdapter<AlarmSound>(this,
                android.R.layout.simple_dropdown_item_1line, sounds);

        spinner.setAdapter(adapter);

        AdapterView.OnItemSelectedListener itemListener = new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View v, int position, long id) {
                AlarmSound item = (AlarmSound) parent.getItemAtPosition(position);

                _model.setSongResource(item.getPath());
                _model.setName(item.getName());
            }
            public void onNothingSelected(AdapterView<?> parent) {

            }
        };

        spinner.setOnItemSelectedListener(itemListener);
    }

    private void _setDateToButton(AlarmModel model) {
        DateTime time = model.getTime();
        DateTimeFormatter fmt = DateTimeFormat.forPattern("hh:mm");

        _btn.setText(fmt.print(time));
    }

    private ArrayList<AlarmSound> _getSounds() {
        RingtoneManager manager = new RingtoneManager(this);
        manager.setType(RingtoneManager.TYPE_ALARM);
        Cursor cursor = manager.getCursor();

        ArrayList<AlarmSound> list = new ArrayList<AlarmSound>();
        while (cursor.moveToNext()) {
            String notificationTitle = cursor.getString(RingtoneManager.TITLE_COLUMN_INDEX);
            String notificationUri = cursor.getString(RingtoneManager.URI_COLUMN_INDEX);
            String id = cursor.getString(RingtoneManager.ID_COLUMN_INDEX);

            list.add(new AlarmSound(notificationTitle, notificationUri + "/" + id));
        }

        return list;
    }

    public ArrayList<String> getNotificationSounds() {
        RingtoneManager manager = new RingtoneManager(this);
        manager.setType(RingtoneManager.TYPE_NOTIFICATION);
        Cursor cursor = manager.getCursor();

        ArrayList<String> list = new ArrayList<>();
        while (cursor.moveToNext()) {
            String id = cursor.getString(RingtoneManager.ID_COLUMN_INDEX);
            String uri = cursor.getString(RingtoneManager.URI_COLUMN_INDEX);

            list.add(uri + "/" + id);
        }

        return list;
    }


    private void _readIntent() {
        this._model = (AlarmModel) getIntent().getParcelableExtra(MainActivity.TIME_MESSAGE);
        this._position = getIntent().getIntExtra(MainActivity.POSITION_MESSAGE, -1);

        if (_model == null) {
            _model = new AlarmModel("", new DateTime());
        }
    }

    public void openTimeDialog(View view) {
        TimePickerDialog timePickerDialog = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                AlarmForm.this._model.setTime(new DateTime()
                        .withHourOfDay(hourOfDay)
                        .withMinuteOfHour(minute));

                _setDateToButton(_model);
            }
        }, _model.getTime().getHourOfDay(), _model.getTime().getMinuteOfHour(), true);

        timePickerDialog.show();
    }

    public void save(View view) {
        Intent data = new Intent();
        data.putExtra(MainActivity.POSITION_MESSAGE, _position);
        data.putExtra(MainActivity.TIME_MESSAGE, _model);
        setResult(RESULT_OK, data);
        finish();
    }

    private TimePicker timePicker;
}
