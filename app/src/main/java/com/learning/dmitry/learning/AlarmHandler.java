package com.learning.dmitry.learning;

import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.view.WindowManager;
import android.widget.Toast;

import net.danlew.android.joda.JodaTimeAndroid;

public class AlarmHandler extends BroadcastReceiver {
    public final static String TimerExtraString = "TIMER_EXTRA_STRING";

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent == null) {
            Toast.makeText(context, AlarmHandler.TimerExtraString, Toast.LENGTH_LONG).show();
        }

        JodaTimeAndroid.init(context);

        AlarmModel model = (AlarmModel) AlarmHandler.unpack(intent.getByteArrayExtra(AlarmHandler.TimerExtraString), AlarmModel.CREATOR);

        if (model == null) {
            return;
        }

        Toast.makeText(context, model.getName(), Toast.LENGTH_LONG).show();

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        final MediaPlayer player = this.playSound(context, Uri.parse(model.getSongResource()));

        builder.setCancelable(false)
                .setTitle(model.getName())
                .setNegativeButton("Отключить", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        player.stop();
                    }
                });

        AlertDialog alert = builder.create();

        try {
            if (alert.getWindow() != null) {
                alert.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
                alert.show();
            }
        } catch (Exception ex) {
        }
    }

    public MediaPlayer playSound(Context context, Uri path) {

        MediaPlayer mMediaPlayer = MediaPlayer.create(context, path);

        mMediaPlayer.start();

        return mMediaPlayer;
    }

    public static byte[] pack(Parcelable parcelable) {
        Parcel parcel = Parcel.obtain();
        parcelable.writeToParcel(parcel, 0);
        byte[] bytes = parcel.marshall();
        parcel.recycle();
        return bytes;
    }

    public static <T> T unpack(byte[] bytes, Parcelable.Creator<T> creator) {
        Parcel parcel = Parcel.obtain();
        parcel.unmarshall(bytes, 0, bytes.length);
        parcel.setDataPosition(0);
        return creator.createFromParcel(parcel);
    }

    public void setAlarm(Context context, AlarmModel model, boolean isCancel) {
        if (model == null) {
            return;
        }

        AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);

        Intent intent = new Intent(context, AlarmHandler.class);
        intent.putExtra(AlarmHandler.TimerExtraString, AlarmHandler.pack(model));

        PendingIntent pi = PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        if (!isCancel) {
            _setAlarm(model, am, pi);
        } else {
            _cancelAlarm(am, pi);
        }
    }

    private void _setAlarm(AlarmModel model, AlarmManager am, PendingIntent pi) {
        am.set(AlarmManager.RTC_WAKEUP, model.getTime().getMillis(), pi);
    }


    private void _cancelAlarm(AlarmManager am, PendingIntent pi) {
        am.cancel(pi);
    }
}