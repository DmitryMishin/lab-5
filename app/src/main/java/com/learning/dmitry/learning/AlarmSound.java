package com.learning.dmitry.learning;

/**
 * Created by Dmitry on 19.04.17.
 */

public class AlarmSound {
    private String _name;
    private String _path;

    public AlarmSound(String name, String path) {
        _name = name;
        _path = path;
    }

    public String getName() {
        return _name;
    }

    public void setName(String _name) {
        this._name = _name;
    }

    public String getPath() {
        return _path;
    }

    public void setPath(String _path) {
        this._path = _path;
    }

    @Override
    public String toString() {
        return _name;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof AlarmSound){
            AlarmSound c = (AlarmSound) obj;
            if(c.getName().equals(_name) && c.getPath() == _path)
                return true;
        }

        return false;
    }
}
