package com.learning.dmitry.learning;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import net.danlew.android.joda.JodaTimeAndroid;

import org.joda.time.DateTime;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity {
    public final static String TIME_MESSAGE = "TIME";
    public final static String POSITION_MESSAGE = "POSITION";
    public final static String ADD_MESSAGE = "ADD_TIME";

    private AlarmHandler _receiver;

    private final static int REQUEST_PUT_ACCESS_TYPE = 1;
    private final static int REQUEST_POST_ACCESS_TYPE = 2;

    private List<AlarmModel> times = new ArrayList();

    ListView timesList;
    AlarmAdapter alarmAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        JodaTimeAndroid.init(this);
        super.onCreate(savedInstanceState);

        _receiver = new AlarmHandler();

        setContentView(R.layout.activity_main);

        setInitialData();

        timesList = (ListView) findViewById(R.id.timesList);
        alarmAdapter = new AlarmAdapter(this, R.layout.alarm_adapter, times);
        timesList.setAdapter(alarmAdapter);

        AdapterView.OnItemClickListener itemListener = new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                AlarmModel item = (AlarmModel) parent.getItemAtPosition(position);
                Intent intent = new Intent(MainActivity.this, AlarmForm.class);

                intent.putExtra(TIME_MESSAGE, item);
                intent.putExtra(POSITION_MESSAGE, position);
                startActivityForResult(intent, REQUEST_PUT_ACCESS_TYPE);
            }
        };

        boolean check = ContextCompat.checkSelfPermission(this,
                Manifest.permission.SYSTEM_ALERT_WINDOW)
                != PackageManager.PERMISSION_GRANTED;
        if (!check) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.SYSTEM_ALERT_WINDOW},
                    1);
        }
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            Intent myIntent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION);
            startActivity(myIntent);
        }

        timesList.setOnItemClickListener(itemListener);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    public void handleAddTime() {
        AlarmModel time = new AlarmModel("", new DateTime());
        Intent intent = new Intent(this, AlarmForm.class);

        intent.putExtra(TIME_MESSAGE, time);

        startActivityForResult(intent, REQUEST_POST_ACCESS_TYPE);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_adding_time:
                handleAddTime();
                return true;

            default:
                return super.onOptionsItemSelected(item);

        }
    }

    private void _updateTimeItems() {
        alarmAdapter.notifyDataSetChanged();
    }

    public void removeTime(View view) {
        final int position = timesList.getPositionForView((View) view.getParent());

        AlarmModel model = times.get(position);

        _receiver.setAlarm(getApplicationContext(), model, true);

        times.remove(position);
        _updateTimeItems();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data){
        if(requestCode == REQUEST_PUT_ACCESS_TYPE) {
            if (resultCode == RESULT_OK) {
                int positionMessage = data.getIntExtra(POSITION_MESSAGE, -1);

                if (positionMessage != -1) {
                    AlarmModel time = this.times.get(positionMessage);
                    AlarmModel inputTime = (AlarmModel) data.getParcelableExtra(TIME_MESSAGE);

                    _receiver.setAlarm(MainActivity.this, time, true);
                    _receiver.setAlarm(MainActivity.this, inputTime, false);
                    times.set(positionMessage, inputTime);

                    _updateTimeItems();
                }
            }
        } else if (requestCode == REQUEST_POST_ACCESS_TYPE) {
            if (resultCode == RESULT_OK) {
                AlarmModel time = (AlarmModel) data.getParcelableExtra(TIME_MESSAGE);

                _receiver.setAlarm(MainActivity.this, time, false);
                times.add(time);
                _updateTimeItems();
            }
        }
        else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void setInitialData(){
    }
}
