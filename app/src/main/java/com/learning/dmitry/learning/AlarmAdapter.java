package com.learning.dmitry.learning;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

public class AlarmAdapter extends ArrayAdapter<AlarmModel> {
    private LayoutInflater inflater;
    private int layout;
    private List<AlarmModel> models;

    public AlarmAdapter(Context context, int resource, List<AlarmModel> models) {
        super(context, resource, models);

        this.models = models;
        this.layout = resource;
        this.inflater = LayoutInflater.from(context);
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View view = inflater.inflate(this.layout, parent, false);

        TextView nameView = (TextView) view.findViewById(R.id.name);
        TextView capitalView = (TextView) view.findViewById(R.id.time);

        AlarmModel time = models.get(position);

        nameView.setText(time.getName());
        capitalView.setText(time.getTime().toString());

        return view;
    }
}
