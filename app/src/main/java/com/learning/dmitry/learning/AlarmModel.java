package com.learning.dmitry.learning;

import android.os.Parcel;
import android.os.Parcelable;

import org.joda.time.DateTime;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class AlarmModel implements Parcelable {
    private String _name;
    private DateTime _time;
    private String _resource;

    public AlarmModel(String name, DateTime time) {
        this._name = name;
        this._time = time;
        if(_time == null) {
            _time = new DateTime();
        }

        if(_name == null) {
            _name = "";
        }

        this._name = name;
        this._time = time;
    }

    public AlarmModel(Parcel in) {
        String[] data = new String[3];

        in.readStringArray(data);

        this._time = (new DateTime(Long.parseLong(data[0])));
        this._name = data[1];
        this._resource = data[2];
    }

    public String getName() {
        return _name;
    }

    public void setName(String name) {
        this._name = name;
    }

    public DateTime getTime() {
        return _time;
    }

    public void setTime(DateTime time) {
        this._time = time;
    }

    public String getSongResource() {
        return _resource;
    }

    public void setSongResource(String resource) {
        this._resource = resource;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AlarmModel that = (AlarmModel) o;

        if (_name != null ? !_name.equals(that._name) : that._name != null) return false;
        if (_time != null ? !_time.equals(that._time) : that._time != null) return false;
        return _resource != null ? _resource.equals(that._resource) : that._resource == null;

    }

    @Override
    public int hashCode() {
        int result = _name.hashCode();
        result = 31 * result + _time.hashCode();
        return result;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeStringArray(new String[] {
                Long.toString(_time.getMillis()),
                _name,
                _resource
        });
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public AlarmModel createFromParcel(Parcel in) {
            return new AlarmModel(in);
        }

        public AlarmModel[] newArray(int size) {
            return new AlarmModel[size];
        }
    };

    private void writeObject(ObjectOutputStream o)
            throws IOException {

        o.writeLong(_time.getMillis());
        o.writeObject(_name);
        o.writeObject(_resource);
    }

    private void readObject(ObjectInputStream o)
            throws IOException, ClassNotFoundException {

        _time = new DateTime(o.readLong());
        _name = (String) o.readObject();
        _resource = (String) o.readObject();
    }
}
